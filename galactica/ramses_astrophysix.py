# coding: utf-8


from astrophysix.simdm.protocol import (
    Algorithm,
    AlgoType,
    InputParameter,
    PhysicalProcess,
    Physics,
    SimulationCode,
)

# Simulation code definition  #
ramses = SimulationCode(
    name="Ramses 3 (MHD)",
    code_name="Ramses",
    code_version="3.10.1",
    alias="RAMSES_3",
    url="https://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html",
    description="Ramses MHD code",
)
# => Add algorithms : available algorithm types are :
# - AlgoType.AdaptiveMeshRefinement
# - AlgoType.VoronoiMovingMesh
# - AlgoType.SmoothParticleHydrodynamics
# - AlgoType.Godunov
# - AlgoType.PoissonMultigrid
# - AlgoType.PoissonConjugateGradient
# - AlgoType.ParticleMesh
# - AlgoType.FriendOfFriend
# - AlgoType.HLLCRiemann
# - AlgoType.RayTracer
ramses.algorithms.add(
    Algorithm(algo_type=AlgoType.AdaptiveMeshRefinement, description="AMR")
)
ramses.algorithms.add(
    Algorithm(algo_type=AlgoType.Godunov, description="Godunov scheme")
)
ramses.algorithms.add(
    Algorithm(algo_type=AlgoType.HLLCRiemann, description="HLLC Riemann solver")
)
ramses.algorithms.add(
    Algorithm(
        algo_type=AlgoType.PoissonMultigrid, description="Multigrid Poisson solver"
    )
)
# => Add input parameters
ramses.input_parameters.add(
    InputParameter(
        key="amr_params/levelmin",
        name="lmin",
        description="min. level of AMR refinement",
    )
)
ramses.input_parameters.add(
    InputParameter(
        key="amr_params/levelmax",
        name="lmax",
        description="max. level of AMR refinement",
    )
)
ramses.input_parameters.add(
    InputParameter(
        key="amr_params/jeans_refine",
        name="jeans_refine",
        description="Array, number of cells needed to resolve the Jeans lenght at each level from lmin",
    )
)
ramses.input_parameters.add(
    InputParameter(
        key="cloud_params/beta_cool", name="beta", description="Cooling parameter"
    )
)


# => Add physical processes : available physics are :
# - Physics.SelfGravity
# - Physics.Hydrodynamics
# - Physics.MHD
# - Physics.StarFormation
# - Physics.SupernovaeFeedback
# - Physics.AGNFeedback
# - Physics.MolecularCooling
ramses.physical_processes.add(
    PhysicalProcess(
        physics=Physics.Hydrodynamics, description="Hydrodynamical equations are solved"
    )
)
ramses.physical_processes.add(
    PhysicalProcess(physics=Physics.SelfGravity, description="Self-Gravity is applied.")
)
