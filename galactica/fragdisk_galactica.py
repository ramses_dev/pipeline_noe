#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function, unicode_literals

import os
import h5py
import numpy as np
import matplotlib as mpl
from astrophysix import units as U
from astrophysix.simdm import Project, ProjectCategory, SimulationStudy
from astrophysix.simdm.datafiles import PlotInfo, PlotType
from astrophysix.utils.file import FileType

from plotter import Plotter
from params import default_params
from ramses_astrophysix import ramses


# ---------------------------------------------- Global parameters ------------------------------------------ #

# groups = ["jr13_tic"]
groups = ["jr11", "jr12", "jr12_tic", "jr13_tic"]
keep_plot_info = False
include_hdf5 = True
replot = True
nml_key = "cloud_params/beta_cool"
select = {
    "time": 4.5,
    #    "filter_nml" : (nml_key, "=", 8),
}


params = default_params()
params.input.nml_filename = "disk.nml"
params.pymses.map_size = 2048
params.pymses.zoom = 4
params.pymses.filter = False
params.pymses.variables = ["rho", "vel", "P", "g"]
params.pymses.multiprocessing = True
params.process.verbose = True
params.disk.enable = True
params.disk.nb_bin = 100
params.pdf.nb_bin = 100
params.process.num_process = 10
in_dir = "/drf/projets/alfven-data/nbrucy/simus/fragdisk"
out_dir = "/dsm/anais/storageA/nbrucy/visus/fragdisk/mnras"
in_dir_conv = "/drf/projets/alfven-data/nbrucy/simus/conv_disk"
out_dir_conv = "/dsm/anais/storageA/nbrucy/visus/conv_disk"


# ---------------------------------------------- Project creation ------------------------------------------ #
# Available project categories are :
# - ProjectCategory.SolarMHD
# - ProjectCategory.PlanetaryAtmospheres
# - ProjectCategory.StarPlanetInteractions
# - ProjectCategory.StarFormation
# - ProjectCategory.Supernovae
# - ProjectCategory.GalaxyFormation
# - ProjectCategory.GalaxyMergers
# - ProjectCategory.Cosmology
proj = Project(
    category=ProjectCategory.StarPlanetInteractions,
    project_title="Fragdisk",
    alias="FRAGDISK",
    short_description="Fragmentation of self-gravitating disks",
    general_description="""

    <h4> Study of the fragmentation of self-gravitating disks</h4>
    <p>
    See Brucy & Hennebelle 2021 (submitted) for more details.
    This database is currently being completed.
    </p>

    <h4>Abstract:</h4>

    <p>
    Self-gravitating disks are believed to play an important role in astrophysics in particular regarding the
    star and planet formation process.
    In this context, disks subject to an idealized cooling process, characterized by a cooling timescale
    $\\beta$ expressed in unit of orbital timescale, have been extensively studied.
    We take advantage of the Riemann solver and the 3D Godunov scheme implemented in the code Ramses to
    perform high resolution simulations, complementing previous studies that have used Smoothed Particle
    Hydrodynamics (SPH) or 2D grid codes.
    </p>

   <h4> Description of the simulations: </h4>

    <p>
    We simulate a disk of gas undergoing purely hydrodynamics forces,
    its own gravity and the $\\beta$-cooling.
    The simulation is ran with the 3D-grid code Ramses (Teyssier 2002) which uses a Godunov scheme.
    The flux between each cell is computed with the HLLC Riemann solver.
    The gravity potential is updated at each timestep with a Poisson solver,
    and a source term is added to the energy equation to implement the $\\beta$-cooling.
    </p>

    <p>
    The $\\beta$-cooling consists in removing internal energy from the gas with a cooling time:
    $t_\\text{cool} = \\beta \\Omega^{-1}$ with $\\Omega$ the rotation frequency.
    </p>

    <p>
    We use the same initial conditions as in Meru & Bate (2012) to allow comparison.
    The specific disk setup for Ramses was inspired by Hennebelle et al. (2017).
    The disk is initially close to equilibrium with an initial column density profile
    $\\Sigma \\propto r^{-1}$ and a temperature profile $T \\propto r^{-1/2}$
    where $r$ is the cylindrical radius.
    The disk has a radius $r_d = 0.25$ (code units), after which the density is divided by 100.
    The density and temperature at the disk radius $r_d$ are chosen so that the mass
    of the disk $M_d = 0.1 M_\\star$, where $M_\\star$ is the mass of the central object,
    and the initial value of the Toomre parameter at the disk radius is $Q_{0,d} = 2$.
    The adiabatic index of the gas is $\\gamma = 5/3$.
    </p>

    <p>
    The simulation is run within a cube of size $L=2$. Although the problem has a cylindrical symmetry,
    we use Cartesian coordinates.
    This prevents having a singularity at the centre of the box.
    One caveat is the poor resolution on the centre of the cube but this is mitigated
    by the use of the adaptive mesh refinement (AMR).
    Another caveat is that having a cubic box may introduce spurious
    reflection at the border of the simulation.
    To avoid this, we maintain a dead zone over a radius of $0.875$ (in code units)
    where all variables are replaced by their initial value at each timestep.
    This method has been used in  Hennebelle et al. (2017) and has proven to be efficient.
    </p>


    <p>
    The simulations presented here were run for several values of $\beta$ and several resolutions.
    To reduce the computation time, we use the Ramses's Adaptative Mesh Refinement (AMR).
    Only the parts of the simulation which are prone to form fragments are simulated with full resolution.
    Each cell is refined until the Jeans's length is covered by at least 20 cells or it reaches the maximum
    level of refinement $l_{\\max}$.
    The level of refinement of a cell is the number of times the simulation box must be divided in eight
    equal part to get the cell.
    Thus, the resolution of a simulation is given by the value of $l_{\\max}$.

    A first set of simulations with $l_{\\max} = 11$ to $l_{\\max} = 12$ are run until
    about 5 Outer Rotation Periods (ORP),
    that is that the gas at the border of the disk had 5 orbits around the star.
    A second set of simulations, labelled tic, for Turbulent Initial Condition,
    were run from relaxed initial conditions for $l_{\\max} = 12$ and $l_{\\max} = 13$.
    More precisely, they were restarted from a simulation at $\beta = 20$ and $l_{\\max} = 12$
    for which the whole disk reached a gravito-turbulent state (after 2 ORPs).
    According to  Paardekooper et al. (2011) and Clarke et al.(2007), departing
    from such turbulent condition should reduce spurious fragmentation.

    </p>
    """,
    data_description="""
    <p>The data available for this project is the underlying data of the article Brucy & Hennebelle 2021.</p>
    <p>The data is not already fully uploaded. 3D datacube extraction on demand is planned</p>
    """,
    directory_path="~nbrucy/simus/fragdisk",
)
print(proj)
# ------------------------------------------------------------------------------------------------------ #


# ---- Units ----
pl_units = Plotter(
    in_dir,
    filter_name="104_beta4_jr13",
    in_nums="last",
    path_out=out_dir,
    params=params,
)
info = pl_units.comp.info
rd = U.Unit.create_unit(
    "r_d",
    latex="r_\\mathrm{d}",
    base_unit=0.25 * info["unit_length"] / info["boxlen"],
)
rhod = U.Unit.create_unit(
    "rho_d",
    latex="\\rho_\\mathrm{d}",
    base_unit=6.36 * info["unit_density"],
)
rho_u = U.Unit.create_unit(
    "rho_u",
    latex="\mathrm{M}_\\star.r_\\mathrm{d}^{-3}",
    base_unit=info["unit_mass"] * rd ** (-3),
)
orp = U.Unit.create_unit(
    "ORP",
    base_unit=2 * np.pi * np.sqrt(0.25 ** 3) * info["unit_time"],
)
vkd = U.Unit.create_unit(
    "vkd",
    latex="v_\\mathrm{k,d}",
    base_unit=2 * np.pi * rd / orp,
)
Pd = U.Unit.create_unit(
    "P_d",
    base_unit=(0.2 * info["unit_velocity"]) ** 2 * rhod,
)
P_u = U.Unit.create_unit(
    "P_u",
    latex="v_\\mathrm{k,d}^2.\\mathrm{M}_\\star.r_\\mathrm{d}^{-3}",
    base_unit=vkd ** 2 * rho_u,
)

Sigmad = U.Unit.create_unit(
    "Sigma_d",
    latex="\\Sigma_d",
    base_unit=0.25 * info["unit_density"] * info["unit_length"] / info["boxlen"],
)
Sigma_u = U.Unit.create_unit(
    "Sigma_u",
    latex="M_\\star.r_d^{-2}",
    base_unit=rd ** (-2) * info["unit_mass"],
)


# ---- Runs  -----

pls = []

for group in groups:
    if group == "jr13_tic":
        # JR13_TIC

        params.astrophysix.simu_fmt = "beta{nml[cloud_params/beta_cool]:g}_{tag:.8}"
        params.astrophysix.descr_fmt = """
        <p>Group {tag:.8}, $\\beta$ = {nml[cloud_params/beta_cool]}.</p>
        """

        runs = "*_jr13"
        pl = Plotter(
            in_dir,
            filter_name=runs,
            in_nums="all",
            sort_run_by=nml_key,
            path_out=out_dir,
            tag="jr13_tic_mnras",
            params=params,
            unit_time=orp,
        )

    if group == "jr12_tic":
        # JR12_TIC
        params.astrophysix.simu_fmt = "beta{nml[cloud_params/beta_cool]:g}_{tag:.8}"
        params.astrophysix.descr_fmt = """
        <p>Group {tag:.8}, $\\beta$ = {nml[cloud_params/beta_cool]}.</p>
        """
        runs_12 = "0[0-9][0-9]_beta*_jr12"
        pl = Plotter(
            in_dir,
            filter_name=runs_12,
            in_nums="all",
            sort_run_by=nml_key,
            filter_nml=("cloud_params", "!=", 7),
            path_out=out_dir,
            tag="jr12_tic_mnras",
            params=params,
            unit_time=orp,
        )

    if group == "jr12":

        params.astrophysix.simu_fmt = "beta{nml[cloud_params/beta_cool]:g}_{tag:.4}"
        params.astrophysix.descr_fmt = """
        <p>Group {tag:.4}, $\\beta$ = {nml[cloud_params/beta_cool]}</p>
        """

        # JR12
        runs = "[7-8][0-9]_beta*_j*"
        pl = Plotter(
            in_dir_conv,
            filter_name=runs,
            in_nums="all",
            sort_run_by=nml_key,
            path_out=out_dir,
            tag="jr12_mnras",
            params=params,
            unit_time=orp,
        )

    if group == "jr11":

        params.astrophysix.simu_fmt = "beta{nml[cloud_params/beta_cool]:g}_{tag:.4}"
        params.astrophysix.descr_fmt = """
        <p>Group {tag:.4}, $\\beta$ = {nml[cloud_params/beta_cool]}</p>
        """

        # JR11
        runs = "*beta*_jr11"
        pl = Plotter(
            in_dir_conv,
            filter_name=runs,
            sort_run_by=nml_key,
            filter_nml=("cloud_params/beta_cool", ">", 3),
            path_out=out_dir_conv,
            tag="jr11_mnras",
            params=params,
            unit_time=orp,
        )

    pls.append(pl)
    print("{} defined".format(group.upper()))

# -------------------------------------------------------------------------------------------------------------------- #

for pl in pls:
    pl.params.process.verbose = True
    pl.comp.params.process.verbose = True
    for run in pl.runs:
        simu = pl.simulations[run]
        proj.simulations.add(simu)


# -------------------------------------------------------------------------------------------------------------------- #


for pl in pls:
    # Edit descriptions
    pl.rules["slice_rho"].description = "Density slice"
    pl.rules["slice_P"].description = "Pressure slice"
    pl.rules["slice_velr"].description = "Radial velocity slice"
    pl.rules["slice_velphi"].description = "Orthoradial velocity slice"
    pl.rules[
        "pdf_coldens"
    ].description = """
    Probability function of the logarithm of the column density fluctuations
    $\\sigma = \\Sigma/\\overline{\\Sigma}$ with respect to its azimuthal average
    """

    # define plot parameters
    map_kwargs = {
        "overwrite": replot,
        "overwrite_dep": False,
        "unit_space": rd,
        "center_space": True,
        "unit_time": orp,
        "nml_key": "cloud_params/beta_cool",
        "select": select,
    }

    coldens_kwargs = {
        **map_kwargs,
        **{
            "unit": Sigma_u,
            "label": r"$\Sigma$",
        },
    }

    rho_kwargs = {
        **map_kwargs,
        **{
            "unit": rho_u,
            "label": r"$\rho$",
        },
    }

    P_kwargs = {
        **map_kwargs,
        **{
            "unit": P_u,
            "label": r"$P$",
        },
    }

    vel_kwargs = {
        **map_kwargs,
        **{
            "unit": vkd,
        },
    }

    # do plots
    pl.coldens("z", **coldens_kwargs)
    pl.coldens("y", **coldens_kwargs)

    pl.slice_rho("z", **rho_kwargs)
    pl.slice_rho("y", **rho_kwargs)

    pl.slice_velr(
        "z",
        label=r"$v_r$",
        norm=mpl.colors.SymLogNorm(0.1, vmin=-2, vmax=2, base=10),
        autoscale=False,
        **vel_kwargs,
    )
    pl.slice_velphi("z", label=r"$v_\varphi$", **vel_kwargs)
    pl.slice_velx("z", label=r"$v_x$", **vel_kwargs)

    pl.slice_P("z", **P_kwargs)

    pl.pdf_coldens(
        "z",
        overwrite=replot,
        overwrite_dep=False,
        unit_time=orp,
        nml_key="cloud_params/beta_cool",
        label=r"$\log(\Sigma / \overline{\Sigma})$",
        kind="step",
        color="k",
        select=select,
    )


# ----------------------------------------------------------------------------------------------------------------- #

# Light fake PlotInfo to replace real one to reduce size of the hdf5 file
pi = PlotInfo(
    plot_type=PlotType.LINE_PLOT,
    xaxis_values=np.array([10.0, 20.0, 30.0, 40.0, 50.0]),
    yaxis_values=np.array([1.256, 2.456, 3.921, 4.327, 5.159]),
    xaxis_log_scale=False,
    yaxis_log_scale=False,
    xaxis_label="Mass",
    yaxis_label="Probability",
    xaxis_unit=U.Msun,
    plot_title="Initial mass function",
    yaxis_unit=U.Mpc,
)

for simu in proj.simulations:
    for snap in simu.snapshots:

        # Change unit to things Galactica understands
        snap.time = (snap.time[0], U.none)
        snap.description = """
        <p>Snapshot at {time:.3g} ORPs.</p>

        <h5 class="sn_panel">Notations:</h5>

        <p>
        $r_d$ : radius of the disk <br />
        ORP : outer rotation period, that is period of the gas at $r_d$ <br />
        $M_\\star$ : Mass of the central object <br />
        $v_{kepl}$ : Keplerian speed at $r_d$
        </p>
        <p>All slices are taken at $z=0$ or $y=0$.
        """.format(
            time=snap.time[0], kepl="{k,d}"
        )

        # Convert plotinfo into HDF5 file
        for df in snap.datafiles:
            name = df[FileType.JPEG_FILE].filename
            name = os.path.splitext(name)[0] + ".h5"
            if include_hdf5 and df.name not in ["slice_velphi_z", "slice_velr_z"]:
                h5 = h5py.File(out_dir + "/" + name, "w")
                p = h5.create_group("plot")
                df.plot_info.hsp_save_to_h5(p)
                h5.close()
                df[FileType.HDF5_FILE] = out_dir + "/" + name
            if not keep_plot_info:
                df.plot_info = pi

# Trim parameters name
for param in ramses.input_parameters:
    param.key = os.path.basename(param.key)

# Validity check
proj.galactica_validity_check()

# Save
study = SimulationStudy(project=proj)
study.save_HDF5(out_dir + "/fragdisk_study.h5")
