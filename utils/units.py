# coding: utf-8

import astrophysix.units as U

create_unit = U.Unit.create_unit


def parse_exp_unit(u):
    splitted = u.split("^")
    name_u = U.Unit.from_name(splitted[0]).latex.replace("text", "math")
    exp = ""
    if len(splitted) > 1:
        exp = "^{" + str(splitted[1]) + "}"
    return name_u + exp


def convert_exp(number, digits=4):
    # Split string as [coeff, exponent]
    splitted = "{num:.{digits}g}".format(num=number, digits=digits).split("e")
    # If no need of scientific notation (low number of digits)
    if len(splitted) == 1:
        return "{}".format(splitted[0])
    else:
        coeff = splitted[0]
        exp = splitted[1]
        exp_str = "10^{" + str(int(exp)) + "}"
        if float(coeff) == 1.0:
            return f"${exp_str}$"
        else:
            return f"${coeff}\\times {exp_str}$"


def unit_str(unit, base=None, prefix="", format=" [{unit}]"):
    """
    Format a unit in matplotlib parsable latex

    unit : astrophysics.units.unit.Unit
    base : astrophysics.units.unit.Unit to use as base unit (if None `unit` is used)
    prefix : str to put befor the unit
    format : str with the {unit} key, to put external decoration
    """
    if unit == U.none:
        return ""
    elif base is not None:
        coeff = unit.express(base)
        return unit_str(base, prefix=convert_exp(coeff)+" ")
    elif len(unit.latex) > 0:
        if "." in unit.latex or "^" in unit.latex:
            base_str = ".".join(map(parse_exp_unit, unit.name.split(".")))
            u_str = r"{}${}$".format(prefix, base_str)
        else:
            u_str = r"{}${}$".format(prefix, unit.latex.replace("text", "math"))
    elif len(unit.name) > 0:
        base_str = ".".join(map(parse_exp_unit, unit.name.split(".")))
        u_str = r"{}${}$".format(prefix, base_str)
    else:
        base_str = ".".join(
            map(parse_exp_unit, unit._decompose_base_units().split("."))
        )
        u_str = r"{}${} {}$".format(prefix, unit.coeff, base_str)
    return format.format(unit=u_str)


U.coldens = create_unit(
    "Msun.pc^-2", base_unit=U.Msun / U.pc ** 2, descr="Column density"
)
U.km_s = create_unit("km.s^-1", base_unit=U.km / U.s, descr="Speed")


U.Msun_pc3 = create_unit("Msun.pc^-3", base_unit=U.Msun / U.pc ** 3, descr="Density")

U.kg_m3 = create_unit("kg.m^-3", base_unit=U.kg / U.m ** 3, descr="Density")

U.ssfr = create_unit(
    "Msun.year^-1.pc^-2",
    base_unit=U.Msun / U.year / U.pc ** 2,
    descr="Surfacic SFR",
)
# latex='M$_{\odot}$.yr$^{-1}$.pc$^{-2}$')

U.ssfrG = create_unit(
    "Msun.Gyr^-1.pc^-2",
    base_unit=1e-9 * U.Msun / U.year / U.pc ** 2,
    descr="Surfacic SFR",
    latex="\mathrm{M}_{\odot}.\mathrm{Gyr}^{-1}.\mathrm{pc}^{-2}",
)


U.uG = create_unit(
    "μG", base_unit=1e-10 * U.T, descr="Micro Gauss", latex="\\mu\\mathrm{G}"
)

U.ssfrK = create_unit(
    "Msun.year^-1.kpc^-2",
    base_unit=U.Msun / U.year / U.kpc ** 2,
    descr="Surfacic SFR",
    latex="\mathrm{M}_{\odot}.\mathrm{yr}^{-1}.\mathrm{kpc}^{-2}",
)
