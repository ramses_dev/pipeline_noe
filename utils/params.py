# coding: utf-8

import os
import re

import munch
import yaml

_dir_path = os.path.dirname(os.path.realpath(__file__))

# Add support for '1e3' kind of float
_loader = yaml.SafeLoader
_loader.add_implicit_resolver(
    u"tag:yaml.org,2002:float",
    re.compile(
        u"""^(?:
     [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
    |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
    |\\.[0-9_]+(?:[eE][-+][0-9]+)?
    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
    |[-+]?\\.(?:inf|Inf|INF)
    |\\.(?:nan|NaN|NAN))$""",
        re.X,
    ),
    list(u"-+0123456789."),
)


def params_from_file(filename):
    with open(filename) as f:
        para_disk = yaml.load(f, Loader=_loader)
    return munch.munchify(para_disk)


def default_params():
    return params_from_file(_dir_path + "/../params.yml")


def load_params(filename):
    basic_params = default_params()
    new_params = params_from_file(filename)
    for key in new_params:
        if key in basic_params:
            basic_params[key].update(new_params[key])
    return basic_params
